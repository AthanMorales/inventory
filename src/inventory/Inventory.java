/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory;

/**
 *
 * @author Mauricio Morales
 */
public class Inventory {
    
    //private final List<Item> items;
     /**
     * An array to hold up to 100 items.
     */
    private final static Item[] inventory = new Item[100];
    private int itemCounter = 0; // counts numbers of items in the inventory.


    public int getItemCounter() {
        return itemCounter;
    }
    
    
    
    /**
     * A method to add new item into the inventory.
     *
     * @param newItem the item object to be added into the inventory.
     */
    public void addItem(Item newItem) {
        inventory[itemCounter] = newItem;
         System.out.println("Estas aquei" + itemCounter);
        itemCounter++;
        
        
        
        //quantity++;
    }
    
     /**
     * A method to print full inventory i.e. item's ID, name and it's quantity.
     */
    public void printInventory() {
        for (int i = 0; i < itemCounter; i++) {
            System.out.println("ID: " + inventory[i].getItemID()
                    + "\t Name: " + inventory[i].getName()
                    + "\t Quantity:" + inventory[i].getQuantity());
        }
    }  
    
    /**
     * A method to get quantity of a specific item in the inventory.
     *
     * @param ID the ID of the item to be searched
     * @return the quantity of the item.
     */
    public int getItemQuantity(int ID) {
        int temp = 0;
        for (int j = 0; j < itemCounter; j++) {
            if (inventory[j].getItemID() == ID) {
                temp = inventory[j].getQuantity();
                break;
            }
        }
        return temp;
    }
}
